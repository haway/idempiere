# JasperSoft Studio

## JaperSoft Studio

### Install Font

- Windows -> Preferences
- Jaspersoft Studio -> Fonts -> Add 
- "Family name": ArialUnicodeMS
- TrueTyp (.ttf)"/home/haway/project/idempiere/ARIALUNI.TTF"
- "PDF Encoding": Identity-H (Unicode with horizontal writing)
- Save

### New project

### Link to your database

### Read fields

### Design Report


## Report & Process

- New Report
- "Name":My User List
- "Search Key": My User List
- "Data Access Level": Client+Org
- Click the checkbox "Report"
- Click 上方迴紋針，把 jrxml 檔上傳，範例是用 Blank_A4.jrxml 檔
- "Jasper Report": attachment:Blank_A4.jrxml

## Menu

- New Menu
- "Name": Report-User List
- "Action": Report
- "Prcess": My User List_My User List