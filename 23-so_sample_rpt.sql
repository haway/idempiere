CREATE OR REPLACE FUNCTION so_sample_rpt(pinstance numeric)
RETURNS void AS
$BODY$
DECLARE
/**
p--- ERP 帶入的參數
v--- procedure 運算用的變數 
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);
sqldel VARCHAR (2000);

p RECORD;
r RECORD;
p_DateAcct DATE;
p_DateAcct_TO DATE;
p_DateStep DATE;
p_DateFrom DATE;
p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_Org_ID_TO NUMERIC(10) := 0;
p_IsSOTrx VARCHAR (1) := 'Y';
p_C_DocType_ID NUMERIC(10) := 0;
p_C_DocType_ID_2 NUMERIC(10) := 0;

p_C_BPartner_Value VARCHAR (40) := 0;
p_C_DocType_Value VARCHAR (40) := 0;
p_C_BPartner_Value_TO VARCHAR (40) := '';
p_C_DocType_Value_TO VARCHAR (40) := '';

p_MovementDate DATE;
p_MovementDate_TO DATE;
p_DateInvoiced DATE;
p_DateInvoiced_TO DATE;

v_message VARCHAR (400) := '';
v_documentno VARCHAR (30) := '';
v_count NUMERIC(10) := 0;
v_NextNo NUMERIC(10) := 0;

BEGIN

IF pinstance is null THEN 
pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='Parameter loading';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM ad_pinstance pi
LEFT OUTER JOIN ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
 p_DateAcct_TO = p.p_date_to;
ELSIF p.parametername = 'AD_Org_ID' THEN p_AD_Org_ID = p.p_number;
 p_AD_Org_ID_TO = p.p_number_to;
ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
END IF;

END LOOP;

IF p_User_ID IS NULL THEN 
p_User_ID := 0; 
END IF;

-- 測試用
IF pinstance = 1000000 THEN

p_DateAcct := '2018-01-01';
p_DateAcct_TO := '2018-01-19';
---p_AD_User_ID := 1000784;
END IF;

v_message :='Start Process';

TRUNCATE t_salesamt;
/* 保留字需要雙引號  */
/* 測試 CODE
select  so_sample_rpt(1000000)
select * from t_salesamt
select  ad_pinstance_id, errormsg from  ad_pinstance where ad_pinstance_id = 1000000
*/

/*
#################################### START NEW ####################################
*/
FOR r IN (

-- drop table t_salesamt
-- CREATE TABLE t_salesamt as
	SELECT 
	0 ad_pinstance_id, 
	o.ad_client_id, 
	o.ad_org_id, 
	o.SalesRep_ID, 
	0 m_product_id,
	0 c_bpartner_id,
	sum(ol.linenetamt) 
	FROM c_orderline ol 
	INNER JOIN c_order o on ol.c_order_id = o.c_order_id 
	GROUP BY o.ad_client_id, o.ad_org_id, o.SalesRep_ID
-- select something

)LOOP
v_message :='LOOP process';

INSERT INTO t_salesamt(
	ad_pinstance_id, ad_client_id, ad_org_id, salesrep_id, m_product_id, c_bpartner_id, sum)
	VALUES (pinstance, r.ad_client_id, r.ad_org_id, r.salesrep_id, r.m_product_id, r.c_bpartner_id, r.sum);

END LOOP;

v_message :='END Process';
/*
#################################### END NEW ####################################
*/

IF pinstance > 0 THEN
BEGIN
UPDATE ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO idempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION so_sample_rpt(numeric)
OWNER TO postgres;