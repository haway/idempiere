# 幫你的通訊錄加入 Like 按鈕

需要以完成 05-Addressbook.md 之前的所有步驟。

影片開始於 2:14:30 
https://www.youtube.com/watch?v=djb6pwZRxwg#t=2h14m30s


## 步驟1, 登入

登入 SuperUser

## 步驟2, 修改 ProcessedOn Button

表單做好之後會發現按鈕並不在表單裡面，須修改：

- 點選左側 "Table and Column"
- 搜尋 "AD_User"
- 按下下方 "Column"
- 按下 "Grid tOggle" 切換成列表模式
- 找到 "AD_User_User/Contact" 的 "ProcessedOn"，點選後，再點下 "Grid tOggle" 切換回表單模式。
- 點選 "Always updatable" 與 選擇 "Toolbar Button" 的 "Window"
- 儲存

![Likebutton-1.png](/images/iDempiere-Likebutton-1.png)


## 步驟 3, 修改表單

如果你之前建立表單的時候，"Window Type" 已經選 "Maintain" 的話，就可省略此步驟。

- 點選左側 "Window, Tab & Field"
- 搜尋 "Address Book"
- 修改 "Window Type" 成 "Maintain"
- 儲存
- 請檢查 "User List" 表單的按鈕是否可以按





