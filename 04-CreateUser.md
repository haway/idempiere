# 建立員工資料

從影片 30 分 45 秒開始，點連結跳過去就直接開始。

https://www.youtube.com/watch?v=djb6pwZRxwg#t=30m45s


## 步驟 1, 登入

使用 中油(New Client) 登入，選擇繁體中文介面

## Step 2, 建立員工資料

因為使用繁體中文介面的關係，所以搜尋的關鍵字也要變成中文，如果是英文介面的話就要搜尋英文關鍵字。

- 在上方搜尋框中輸入 "用戶"，點選入用戶, 英文版搜尋 "User"
- 在上方工具列中點選 "New"(新增)
- 填入員工資料(練習影片中有填入照片)
- 存檔

![user-1](/images/iDempiere-user-1.png)
 

## 步驟 3, 設定登入權限

如果你要讓這個權限可以登入的話，要設定下方權限，如果不需要的話，作到步驟 2 就可以了。

- 下方 "組織權限"(英文板 User Role) 選擇 "HQ" 

![user-role](/images/iDempiere-user-2.png)

- 下方 "用戶角色" 選擇 "中油 User"

![org-access](/images/iDempiere-user-3.png)

- 設定 "密碼"

記得要建立 5 個左右的員工資料。



