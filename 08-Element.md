# Element 的應用-虛擬欄位

影片在 55 分開始，影片是不同的，請點連結打開：https://www.youtube.com/watch?v=fY71-TeiQ-4#t=55m0s

**Element 對欄位是一對多，Element 一定要存在。**

## 虛擬欄位

## 步驟 1, 新增 Element

- 在左側選單選 "Table and Column" 
- 搜尋 "AD_User"
- 點選下方 "Column"
- 點選新增小圖示來新增一個欄位

![Element-1.png](/images/iDempiere-Element-1.png)

- 用滑鼠按下 "System Element"，進入 Element 之後按下左上角工具列的 "New" 新增小圖示。
- 在 "DB Column Name" 與 "Name" 與 "Print Text" 都輸入 "BirthdayMMDD"
- 儲存

![Element-2.png](/images/iDempiere-Element-2.png)

- 回到 Column 的新增視窗
- 在 "System Element" 與 "DB Column name" 與 "Name" 都填入 BirthdayMMDD
- 在 "Reference" 選 "String"
- 在 "Length" 選 5
- 在 "Entity Type" 選 "User maintained"
- **在 "Column SQL" 填入下列 SQL：**

    (to_char(AD_User.Birthday, 'MM-dd'))
    
- 存檔，這樣就完成建立了一個虛擬欄位

![Element-3.png](/images/iDempiere-Element-3.png)

## 步驟 2, 變更 Birthday 欄位

- 在左側選單選 "Window, Tab & Field" 
- 搜尋 "Address Book"
- 點選下方 "Tab"
- 點選下方 "Field"
- 點選中間工具列 "Grid tOggle"
- 找到原本的 "Birthday"，點選 "Birthday" 之後在點選上方的 "Grid tOggle"，切換回表單模式
- 變更 "Column" 改成 "BirthdayMMDD_BirthdayMMDD"

![Element-4.png](/images/iDempiere-Element-4.png)



