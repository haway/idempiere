# Install

## Java

### Install Java

簡報的教學:

``` bash
sudo apt install openjdk-11-jdk-headless
sudo apt-get install postgresql-client
wget -O - https://s3-ap-northeast-1.amazonaws.com/aierp/iDempiereTaiwan-0126/setupjdk.sh | bash
```

setupjdk.sh 的內容：
``` bash
#!/bin/bash
# Ninniku Consulting Ray

sudo mkdir -p /usr/local/java
wget https://s3-ap-northeast-1.amazonaws.com/aierp/iDempiereTaiwan-0126/openjdk-11%2B28_linux-x64_bin.tar.gz
sudo cp -r openjdk-11+28_linux-x64_bin.tar.gz /usr/local/java/
cd /usr/local/java
sudo tar xvzf openjdk-11+28_linux-x64_bin.tar.gz

sudo echo "JAVA_HOME=/usr/local/java/jdk-11 " >> /etc/profile
sudo echo "PATH=$PATH:$JAVA_HOME/bin " >> /etc/profile
sudo echo "export JAVA_HOME " >> /etc/profile
sudo echo "export PATH " >> /etc/profile

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/local/java/jdk-11/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/local/java/jdk-11/bin/javac" 1
#sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/local/java/jdk-11/bin/javaws" 1
sudo update-alternatives --set java /usr/local/java/jdk-11/bin/java
sudo update-alternatives --set javac /usr/local/java/jdk-11/bin/javac
#sudo update-alternatives --set javaws /usr/local/java/jdk-11/bin/javaws
echo "Installation Completed!\n"
echo "Please excute [CODE] source /etc/profile [CODE] to enable Java"
```

### 額外的設定

如果發生找不到 jar 的問題，要額外設定 jar 的路徑到 PATH

    export PATH=$PATH/:/usr/local/java/jdk-11/bin/

## 下載 iDepmiere 軟體

投影片教學：
``` bash
mkdir -p /home/idempiere
cd /home/idempiere
wget https://s3-ap-northeast-1.amazonaws.com/aierp/iDempiereTaiwan-0126/idempiere.tar.gz
tar -xzvf idempiere.tar.gz
```

## PostgreSQL

### 使用 Docker PostgreSQL

有兩種方式，第一種是，建立一個資料夾然後把他掛入 Docker 內，這樣每次要用的時候開啟 Docker 就好了

    mkdir -p /home/iDempiere/pgsql_data
    docker run -v /home/iDempiere/pgsql_data:/var/lib/postgresql/data -p 5432:5432  postgres

另外一種是你利用 Docker 消失資料就會刪除的特性，不要把 Volume 掛進去，這樣每次練習完關掉 Docker 就可以**重做**。

    docker run --rm -p 5432:5432 --user postgres postgres
    
## 初始化

需要依照文章的順序執行

### iDempiere

初始化 iDempiere
``` bash
cd /home/idempiere/idempiere 
bash console-setup.sh
Setup idempiere Server
Feb 02, 2019 5:09:29 PM org.compiere.install.ConfigurationData load
INFO: /home/iDempiere/idempiere/idempiereEnv.properties
Java Home [/usr/local/java/jdk-11]:
[enter]

Feb 02, 2019 5:09:34 PM org.compiere.install.ConfigVM test
INFO: OK: JavaHome=/usr/local/java/jdk-11
Feb 02, 2019 5:09:34 PM org.compiere.install.ConfigVM test
INFO: OK: Version=11
iDempiere Home [/home/iDempiere/idempiere]:
Key Store Password [myPassword]:
[enter]

Feb 02, 2019 5:10:02 PM org.compiere.install.KeyStoreMgt <init>
INFO: /home/iDempiere/idempiere/jettyhome/etc/keystore
Feb 02, 2019 5:10:03 PM org.compiere.install.ConfigurationData testAdempiere
INFO: OK: AdempiereHome = /home/iDempiere/idempiere
Feb 02, 2019 5:10:03 PM org.compiere.install.KeyStoreMgt <init>
INFO: /home/iDempiere/idempiere/jettyhome/etc/keystore
Feb 02, 2019 5:10:04 PM org.compiere.install.ConfigurationData testAdempiere
INFO: OK: KeyStore = /home/iDempiere/idempiere/jettyhome/etc/keystore
Application Server Host Name [0.0.0.0]:
0.0.0.0

Application Server Web Port [8080]:
[enter]
Application Server SSL Port[8443]:
[enter]
DB Already Exists?(Y/N) [N]:
n
1. Oracle
2. PostgreSQL
Database Type [2]
2
Database Server Host Name [localhost]:

Database Server Port [5432]:

Database Name[idempiere]:
idempiere
Database user [postgres]:
postgres
Database Password [postgre]:
postgre
Database System User Password []

Feb 02, 2019 5:12:53 PM org.adempiere.db.postgresql.config.ConfigPostgreSQL test
INFO: OK: Database Server = localhost/127.0.0.1
testPort[127.0.0.1, 5432]
Feb 02, 2019 5:12:53 PM org.adempiere.db.postgresql.config.ConfigPostgreSQL test
INFO: OK: Database Port = 5432
Feb 02, 2019 5:12:54 PM org.adempiere.db.postgresql.config.ConfigPostgreSQL test
INFO: OK: System Connection = jdbc:postgresql://localhost:5432/template1
Feb 02, 2019 5:12:54 PM org.adempiere.db.postgresql.config.ConfigPostgreSQL test
INFO: OK: Database User = postgres
Mail Server Host Name [0.0.0.0]:
0.0.0.0
Mail User Login []:

Mail User Password []

Administrator EMail []:

Feb 02, 2019 5:13:00 PM org.compiere.install.ConfigurationData testMail
WARNING: Not valid:  - Illegal address
Save changes (Y/N) [Y]:
Y

```

### 資料庫

初始化 PostgreSQL 資料庫

    cd /home/idempiere/idempiere/utils

下載初始化命令稿：

``` bash
wget https://s3-ap-northeast-1.amazonaws.com/aierp/iDempiereTaiwan-0126/initialdb.sh
bash initialdb.sh
```

initialdb.sh：

``` bash
#!/bin/sh

# $ADEMPIERE_DB_PATH/DBRestore.sh system/$ADEMPIERE_DB_SYSTEM $ADEMPIERE_DB_USER $ADEMPIERE_DB_PASSWORD $ADEMPIERE_DB_SYSTEM
STARTTIME="$(date)"
#PGPASSWORD=postgres
#ADEMPIERE_DB_USER=dev
#ADEMPIERE_DB_PASSWORD=dev
#ADEMPIERE_DB_SERVER=localhost
#ADEMPIERE_DB_PORT=5432
#ADEMPIERE_DB_NAME=dev
#SYSPASSWORD=postgres
#PGPASSWORD=postgres
. ./myEnvironment.sh Server

PGPASSWORD=$ADEMPIERE_DB_PASSWORD
export PGPASSWORD
echo -------------------------------------
echo Recreate user and database
echo -------------------------------------
echo It will delete your current Database $ADEMPIERE_DB_NAME and Database User $ADEMPIERE_DB_USER ...
echo Press enter to continue ...
read in

echo "Drop DB .... "
dropdb -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -U $ADEMPIERE_DB_USER $ADEMPIERE_DB_NAME

PGPASSWORD=$ADEMPIERE_DB_SYSTEM
export PGPASSWORD

dropuser -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -U postgres  $ADEMPIERE_DB_USER


ADEMPIERE_CREATE_ROLE_SQL="CREATE ROLE $ADEMPIERE_DB_USER NOSUPERUSER LOGIN PASSWORD '$ADEMPIERE_DB_PASSWORD'"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -U postgres -c "$ADEMPIERE_CREATE_ROLE_SQL"
ADEMPIERE_CREATE_ROLE_SQL=

#PGPASSWORD=$ADEMPIERE_DB_PASSWORD
#export PGPASSWORD
createdb -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -E UNICODE -O postgres -U postgres $ADEMPIERE_DB_NAME
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "GRANT postgres TO adempiere"

echo -------------------------------------
echo Create Database, Schema , uuid-ossp
echo -------------------------------------

psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "ALTER DATABASE $ADEMPIERE_DB_NAME OWNER TO $ADEMPIERE_DB_USER;"

PGPASSWORD=$ADEMPIERE_DB_PASSWORD
export PGPASSWORD
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "CREATE SCHEMA adempiere;"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "ALTER SCHEMA adempiere OWNER TO $ADEMPIERE_DB_USER;"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "ALTER SCHEMA adempiere OWNER TO postgres;"


PGPASSWORD=$ADEMPIERE_DB_SYSTEM
export PGPASSWORD
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\" WITH SCHEMA adempiere;"

#CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA adempiere;
#psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -f uuid.dmp

psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U postgres -c "ALTER SCHEMA adempiere OWNER TO $ADEMPIERE_DB_USER;"

echo -------------------------------------
echo Import Data base
echo -------------------------------------

PGPASSWORD=$ADEMPIERE_DB_PASSWORD
export PGPASSWORD
ADEMPIERE_ALTER_ROLE_SQL="ALTER ROLE $ADEMPIERE_DB_USER SET search_path TO adempiere, pg_catalog"
psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -c "$ADEMPIERE_ALTER_ROLE_SQL"

echo "Uncompress Database"
cd $IDEMPIERE_HOME/data/seed/
/usr/local/java/jdk-11/bin/jar xvf Adempiere_pg.jar

psql -h $ADEMPIERE_DB_SERVER -p $ADEMPIERE_DB_PORT -d $ADEMPIERE_DB_NAME -U $ADEMPIERE_DB_USER -f Adempiere_pg.dmp
rm  Adempiere_pg.dmp
PGPASSWORD=
export PGPASSWORD
cd $IDEMPIERE_HOME/utils/
echo "from:  $STARTTIME"
echo "End:" "$(date)"

```