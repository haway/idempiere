#

- Install [SQL](https://gitlab.com/RayNinniku/idempiereopencourse/blob/master/procedure/so_invoice_rpt_numeric_.sql)

ad_pinstance_id

## Install 

- Login as System
- Create [T_Invoice table](https://gitlab.com/snippets/1859346)
- 測試: select * from T_Invoice
- Install [so_invoice_rpt](https://gitlab.com/snippets/1859347)


## Create Report & Process

- Login as System
- New Report & Process
- "Search key" : haway_jasper_3
- "Procrudce": so_invoice_rpt
- 上方迴紋針：上傳 aierp_Invoice.jrxml 檔
- "Jasper Report":attachment:aierp_Invoice.jrxml


## Replace reporting in Invoice

- Switch role to GardenWorld
- Search: Invoice (Customer)
- Select any one of invoices
- 修改成自己的格式
- Click on "Document Type"
- Print Format: "Get system if it's empty"
- Click on "Print format" 
- Make a new format.
- Table: C_Invoice_Invoice
- Jasper process: haway_jasper_3

## Jaspersof Studio

m_product_id 





