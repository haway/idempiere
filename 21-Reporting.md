## Change Logo images

- Search "System Configurator"
- 更換Login Image: ZK_LOGO_LARGE
- 更換識別 Image: WEBUI_LOGOURL

## Change Admin Password

## Add your IP Address in firewall of AWS

記得開啟 AWS 的防火牆


## Report

暫存 Table 命名規則：t_xxx

### 本章節步驟

1. 安裝 Procedure
2. 建立表格
3. 建立 Print format
4. 建立 Report
5. 建立 選單

### 安裝 Procedure

Procedure 是 sql 的語法，就是程式的部分，安裝完成後，你會有一個叫 nn_sample_rpt 的 function 會去資料庫抓資料，然會放入 t_salesamt 這個資料表內，我們先建立 t_salesamt 資料表。
**請先用 pgAdmin 單獨執行下面的 SQL 來建立 t_salesamt 資料表。**

````
CREATE TABLE t_salesamt as
SELECT 
0 ad_pinstance_id, 
o.ad_client_id, 
o.ad_org_id, 
o.SalesRep_ID, 
0 m_product_id,
0 c_bpartner_id,
sum(ol.linenetamt) 
FROM c_orderline ol 
INNER JOIN c_order o on ol.c_order_id = o.c_order_id 
GROUP BY o.ad_client_id, o.ad_org_id, o.SalesRep_ID
````

建立完成之後，你可以用 pgAdmin 執行一下下列語法看看有沒有問題，你會得到一個空結果，但是不能出現錯誤。

    select * from t_salesamt

接下來就可以把[整個 Procedure](https://gitlab.com/RayNinniku/idempiere-open-procedure/blob/master/sql/nn_sample_rpt.sql) 貼進去 pgAdmin 執行一次，你就會有一個叫 nn_sample_rpt 的 function。
接下來請用 pgAdmin 跑下列指令：

    select  nn_sample_rpt(1000000)
    
跑完之後再跑一次 `select * from t_salesamt`，這時候 select 完應要有資料。

哈維用的 [Procedure](https://gitlab.com/haway/idempiere/blob/master/23-so_sample_rpt.sql)，**這個 Procedure 我有改過，不適用，請用 Ray 的。**
    

### 2. 建立表格 T_SalesAmt

建立完 `nn_sample_rpt` 與 `t_salesamt` 之後，就可以登入 iDempiere 。

- 登入 System
- "Table & Column" 建立新 Table
- "Name": T_SalesAmt
- "DB Table Name": T_SalesAmt
- "Data Access Level": Client+Org
- Save 
- Click On "gear" in right of top.
- "Create Columns from DB"
- Click "OK" in popup window.

### 建立 Print format

- New Print format
- Name: T_SalesAmt
- Table: T_SalesAmt_T_SalesAmt
- Save
- Click on "gear" in right of top, then "Copy/Create", Table: "T_SalesAmt_T_SalesAmt" then "OK"
- You can choose fields displayed & ordered in below the window after you imported.
- Save

### 建立 Report & Process

- New Report & Process
- "Search Key": nn_sample_rpt
- "Name: nn_sample_rpt
- "Data Access Level": Client+Org
- Active the checkbox "Report"
- "Procedure": nn_sample_rpt
- "Print Format": T_SalesAmt_20190330135140 (你的流水序號可能會跟我的不一樣)
- Save

### 建立 Menu

- New Menu
- "Name": Sales Report
- "Action": Report
- "Process": nn_sample_rpt_nn_sample_rpt

### 增加設定時間區間的功能

- 從 "Sales Order" 中找出我們要用的欄位是 "DateOrdered"
- Search "nn_sample_rpt" in "Report & process"
- New Parameter(按下下方的新增)
- "Name": DateOrdered
- "DB Coulum Name": DateOrdered
- "Reference": Date
- Active the checkbox "Range"
- "Default Logic": @#Date@
- "Default Logic 2":@#Date@

### 完成

按下 Menu 中的 Sales Report 就可以依照時間顯示銷售報表

![圖片1](/images/iDempiere-21-reporting-0.png)

![圖片2](/images/iDempiere-21-reporting-1.png)

## 其他

### SQL

**這是我自己看的章節**

第一個版本的 [SQL](https://gitlab.com/haway/idempiere/blob/master/23-so_sample_rpt.sql)，是不能設定時間區間的，
之後完成了 `增加設定時間區間的功` 的功能後，需要修改 SQL (在課堂上修改的)，
修改的部份我用 `--- NEW ############################################ ` 加註了，如果你是用 Ray 的 SQL，不需要進行這個部份的修改。
但是若是你用 Ray 的 SQL，你必須要在所有步驟都完成(包括"增加設定時間區間的功")之後，介面才會運作。

````
CREATE OR REPLACE FUNCTION so_sample_rpt(pinstance numeric)
RETURNS void AS
$BODY$
DECLARE
/**
p--- ERP 帶入的參數
v--- procedure 運算用的變數 
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);
sqldel VARCHAR (2000);

p RECORD;
r RECORD;

p_DateAcct DATE;
p_DateAcct_TO DATE;
--- NEW ############################################
p_DateOrdered DATE;
p_DateOrdered_TO DATE;
--- NEW ############################################
p_DateStep DATE;
p_DateFrom DATE;

p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_Org_ID_TO NUMERIC(10) := 0;
p_IsSOTrx VARCHAR (1) := 'Y';
p_C_DocType_ID NUMERIC(10) := 0;
p_C_DocType_ID_2 NUMERIC(10) := 0;

p_C_BPartner_Value VARCHAR (40) := 0;
p_C_DocType_Value VARCHAR (40) := 0;
p_C_BPartner_Value_TO VARCHAR (40) := '';
p_C_DocType_Value_TO VARCHAR (40) := '';

p_MovementDate DATE;
p_MovementDate_TO DATE;
p_DateInvoiced DATE;
p_DateInvoiced_TO DATE;

v_message VARCHAR (400) := '';
v_documentno VARCHAR (30) := '';
v_count NUMERIC(10) := 0;
v_NextNo NUMERIC(10) := 0;

BEGIN

IF pinstance is null THEN 
pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='Parameter loading';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM ad_pinstance pi
LEFT OUTER JOIN ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
 p_DateAcct_TO = p.p_date_to;
--- NEW ############################################
ELSIF p.parametername = 'DateOrdered' THEN p_DateOrdered = p.p_date;
 p_DateOrdered_TO = p.p_date_to;
--- NEW ############################################
ELSIF p.parametername = 'AD_Org_ID' THEN p_AD_Org_ID = p.p_number;
 p_AD_Org_ID_TO = p.p_number_to;
ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
END IF;

END LOOP;

IF p_User_ID IS NULL THEN 
p_User_ID := 0; 
END IF;

-- 測試用
IF pinstance = 1000000 THEN

p_DateAcct := '2018-01-01';
p_DateAcct_TO := '2018-01-19';
---p_AD_User_ID := 1000784;
END IF;

v_message :='Start Process';

TRUNCATE t_salesamt;
/* 保留字需要雙引號  */
/* 測試 CODE
select  so_sample_rpt(1000000)
select * from t_salesamt
select  ad_pinstance_id, errormsg from  ad_pinstance where ad_pinstance_id = 1000000
*/

FOR r IN (

-- drop table t_salesamt
-- CREATE TABLE t_salesamt as
	SELECT 
	0 ad_pinstance_id, 
	o.ad_client_id, 
	o.ad_org_id, 
	o.SalesRep_ID, 
	0 m_product_id,
	0 c_bpartner_id,
	sum(ol.linenetamt) 
	FROM c_orderline ol 
	INNER JOIN c_order o on ol.c_order_id = o.c_order_id 
	--- NEW ############################################
	WHERE o.Dateordered between p_DateOrdered and p_DateOrdered_TO
	--- NEW ############################################
	GROUP BY o.ad_client_id, o.ad_org_id, o.SalesRep_ID
-- select something

)LOOP
v_message :='LOOP process';

INSERT INTO t_salesamt(
	ad_pinstance_id, ad_client_id, ad_org_id, salesrep_id, m_product_id, c_bpartner_id, sum)
	VALUES (pinstance, r.ad_client_id, r.ad_org_id, r.salesrep_id, r.m_product_id, r.c_bpartner_id, r.sum);

END LOOP;

v_message :='END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO idempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION so_sample_rpt(numeric)
OWNER TO postgres;
````


