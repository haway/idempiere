# Development Case - Change User Name


https://www.youtube.com/watch?v=7372lJ-TM28&feature=youtu.be

#  New Class

- New Class in tw.haway.testing
- "Name": ChangeName
- "SuperClass": org.compiere.process.SvrProcess

[SourceCode](https://gitlab.com/RayNinniku/idempiereopencourse/blob/master/ChangeName.java)

## 註冊 Process (Adempiere Java Way)

- Click on MAIN-INF -> MAINIFEST.MF -> Extenstions -> Add -> (org.adempiere.base.Process)
- Right clieck on org.adempiere.base.Process -> new -> Process
- "class": tw.haway.testing.process.Changename
- Copy "tw.haway.testing.Debuger" and paster in ID & Name of "Org.adempiere.base.Process"

## 修改 MANIFEST.MF

- Click on "MANIFEST.MF", Open "MANIFEST.MF" Tab
- paste "Eclipse-Registerbuddy: org.adempiere.base" in it.

## 放入 iDempiere 並增加參數

https://www.youtube.com/watch?v=7372lJ-TM28&feature=youtu.be&t=13m25s

- Login in iDempiere with SuperUser
- Repoert & Process
- "Name": ChangeName
- "Classname": tw.haway.testing.process.Changename

### 增加參數

https://www.youtube.com/watch?v=7372lJ-TM28&feature=youtu.be&t=14m30s

新增第一個參數 AD_User_ID

- Click on "Parameter"
- "Name": User/Contact
- "System Element": AD_User_ID
- "DB Column Name": AD_User_ID
- "Reference": SearchKey
- Save

新增第二個參數 Name

- Click on "Parameter"
- "Name": User
- "System Element": Name
- "DB Column Name": Name
- "Reference": String
- Save

新增 Menu

- New Menu
- "Name":ChangeName
- "Action":Process
- "Process":"Change Name_Change Name"
- Save
