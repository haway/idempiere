# Installing Development Enviroment

## 安裝相依軟體

### PostgreSQL

安裝 PostgreSQL 後，修改設定檔：

```
vi /etc/postgresql/(version)/main/postgresql.conf 
listen_addresses = '*'
vim /etc/postgresql/9.4/main/pg_hba.conf
host    all             all              0.0.0.0/0                       md5
host    all             all              ::/0                            md5
```

建立 adempiere 帳號：

```
su - postgres
$ psql

postgres=# CREATE ROLE adempiere SUPERUSER LOGIN PASSWORD 'adempiere';

# 修改 ROLE 具備登入權限
postgres=# ALTER ROLE [Rolename] LOGIN;

# 先建立使用者角色
postgres=# CREATE ROLE adempiere SUPERUSER LOGIN PASSWORD 'adempiere';

# 再建立資料庫並賦予角色
postgres=# CREATE DATABASE idempiere OWNER adempiere;
createdb  --template=template0 -E UNICODE -O adempiere -U adempiere idempiere

# 開權限
postgres=# ALTER ROLE adempiere SET search_path TO adempiere, pg_catalog;

```

測試連線，**一定要測試可以過，再進行下一步**：

```
psql -U adempiere -d idempiere
```

### OpenJDK

安裝 OpenJDK-11.x 

```
sudo apt-get update
sudo apt-get install openjdk-11-jdk
```

或是自己取抓 OpenJDK 回來裝， https://openjdk.java.net/ 。

### 安裝 Maven

```
sudo apt-get install maven
```

## 安裝 Eclipse

請下載 2018-09 的版本，[下載連結](https://www.eclipse.org/downloads/packages/release/2018-09/r)。

接者請安裝 Eclipse 外掛，全部安裝完成之後在重新啟動 Eclipse 即可。

### 外掛 Install Java 11 Support for Eclipse

- Navigate to Help > Install New Software
- Fill the Work with box with the URL http://download.eclipse.org/eclipse/updates/4.9-P-builds/ and push Enter key
- Select the option "Eclipse Java 11 support for 2018-09 development stream"
- Click Next button and then Finish button
- Accept the terms, certificate, etc until installed, no need to restart yet

### Install Mercurial Eclipse Plugin 2.7.0

- Navigate to Help > Eclipse Marketplace
- Fill the Find box with: MercurialEclipse
- Push the "Go" button
- Push the "Install" button in front of the "MercurialEclipse 2.2" found (note this install 2.7.0 despite is named 2.2)
- Accept the terms, certificate, etc until installed, no need to restart yet

### Install Tycho Build Tools

- Navigate to Help > Install New Software
- Fill the Work with box with the URL https://raw.githubusercontent.com/idempiere/binary.file/master/p2-third-party/tycho-build-tools and push Enter key
- Expand "Tycho Build Tools" and select the FOUR plugins: Base Bundle, Mavenize, Rename, Version Tools
- Click Next button and then Finish button
- Accept the terms, certificate, etc until installed, no need to restart yet

### Install Tycho Configurator

- Navigate to Window > Preferences
- Navigate to Maven > Discovery
- Click Open Catalog
- On Find field, enter Tycho
- Select Tycho Configurator, click Finish to install it
- Accept the terms, certificate, etc until installed

重新啟動 Eclipse。

## Doenload Source Code

https://wiki.idempiere.org/en/Download_the_Code

### 建立一個新資料夾，然後 clone source code

建立一個資料夾來存放原始碼。建議再 clone 之後，再複製一份到另一個新資料夾，然後修改都用新的那一份，改壞掉的話在從 source 複製一份就好，不要直接改 source

``` bash
mkdir source
cd source
hg clone https://bitbucket.org/idempiere/idempiere
cp -R source development
cd development
```

### 切換到版本 6.2

```
hg update release-6.2
hg pull -u
```

### 編譯程式碼 

編譯程式碼，**編譯後** 才能匯入 Eclipse 中，沒有編譯之前不要匯入，因為 Eclipse 會更改一些東西。

```
mvn verify -U
```

編譯完成後，二進位檔會放在：

    # linux 64 bit version
    $HOME/sources/testheadless/org.idempiere.p2/target/products/org.adempiere.server.product/linux/gtk/x86_64   

    # windows 64 bit version
    $HOME/sources/testheadless/org.idempiere.p2/target/products/org.adempiere.server.product/win32/win32/x86_64

可參考 [Building_iDempiere_without_Eclipse](https://wiki.idempiere.org/en/Building_iDempiere_without_Eclipse)

## 匯入資料庫

### 初始化資料庫

```
cd /tmp
jar xvf $IDEMPIERE_REPOSITORY/org.adempiere.server-feature/data/seed/Adempiere_pg.jar
psql -d idempiere -U adempiere -f Adempiere_pg.dmp
```

### 升級資料庫

初始化資料庫之後，資料庫還是很舊的版本，需跑一下升級，請把 [這個命令搞](https://bitbucket.org/CarlosRuiz_globalqss/idempiere-stuff/raw/tip/script_to_sync_db/syncApplied.sh) 的第五行改成你的目錄即可。

    MIGRATIONDIR=${3:-~/hgAdempiere/localosgi/migration}

改好之後跑一次就可以。

## 將 iDempiere 匯入 Eclipse

### Import projects in Eclipse

**粗體**的步驟很重要

- Open again the eclipse you configured with the workspace pointing to the folder where you cloned the code
- **Turn off Project > Build automatically**
- Navigate to File > Import
- Navigate to Maven > Existing Maven Projects
- Click Next button and Browse to $IDEMPIERE_REPOSITORY (must appear there by default)
- All projects must appear selected, click Finish to complete the import
- At the end all projects are loaded into workspace

### Workspace settings

- In eclipse, navigate to Window > Preferences > General > Workspace
- set "Text file encoding" to "UTF-8" (if default is different)
- set "New text file line delimiter" to "Unix" (if default is different)

### Set target platform

- Still in eclipse
- Navigate to File > Import
- Navigate to General > Existing Projects into Workspace
- Click Next button and Browse to $IDEMPIERE_REPOSITORY/org.idempiere.p2.targetplatform
- Click Finish to complete the import

    Now open the file org.idempiere.p2.targetplatform.target within the project just imported org.idempiere.p2.targetplatform
        This is the default target platform definition with remote url
        Here eclipse takes some time downloading the required dependencies, you can prepare some Colombian coffee and wait patiently :)
- At Target Editor, click the "Set as Active Target Platform" link
- **Wait for Eclipse to finish downloading bundles onto target platform, it may take few hours, DON'T clicks anything, watch out right of bottom window.**
- **After download completed**, Click the "Reload" button if some download fail, until there are not red errors on the Locations list
- Run -> Run Configure... -> install.app
- Run -> Run Configure... -> server.product

DONE!
