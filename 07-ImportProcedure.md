# 將按鈕加上 Process

影片起始於 2:22:09 https://www.youtube.com/watch?v=djb6pwZRxwg#t=2h22m09s


## 步驟1, 登入

登入 SuperUser

## 步驟2, 匯入 Stored-Procedures

因為我是自己在 Linode 架 PostgreSQL，所以會有一點不一樣，請參考影片的作法。

- 從 [PPT](https://s3-ap-northeast-1.amazonaws.com/aierp/iDempiereTaiwan-0126/hr_good_sp\(numeric\).sql) 下載 SQL.
- 匯入 SQL

### 自架 PostgreSQL 的匯入 Procedure

- 安裝 pgAdmin 4
- 打開 pgAdmin 4
- 點選 Object -> CREATE SCRIPT, 然後把 SQL 貼上去，然後因為我是用 postgres 使用者執行的，所以要把最後的 "adempiere" 改成 "postgres"。

SQL Procedure I **Changed**

``` sql
-- Function: hr_cardnumber_sp(numeric)

-- DROP FUNCTION hr_cardnumber_sp(numeric);

CREATE OR REPLACE FUNCTION hr_good_sp(pinstance numeric)
  RETURNS void AS
$BODY$
DECLARE
/**
出勤異常查詢
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);


p RECORD;
--r RECORD;
v_message text;
p_DateAcct DATE;
p_DateAcct_TO DATE;
p_User_ID NUMERIC(10) := 0;
p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
BEGIN

IF pinstance is null THEN 
	pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='OPEN ITEM Detail';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM adempiere.ad_pinstance pi
LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'DateAcct' THEN p_DateAcct = p.p_date;
     p_DateAcct_TO = p.p_date_to;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;

  END IF;

END LOOP;


IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

IF pinstance = 1000000 THEN
p_Record_ID = 100;
END IF;

v_message :='Start Process';
--EXECUTE sqldel;

 update ad_user set counter = (coalesce(counter,0) + 1 ) where ad_user_id = p_Record_ID;


v_message :='END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO adempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hr_good_sp(numeric)
  OWNER TO postgres;
```

## 步驟3, 新增 Process

- 按下左邊的 "Report & Process"
- 點選 "New" 新增
- 在 "Search_key" 輸入 "hr_good_sp"
- 在 "Name" 輸入 "hr_good_sp"
- 在 "Data Access Level" 選 "Client+Organization"
- 在 "Procedure" 輸入 "hr_good_sp"
- 儲存

![Procedure-1.png](/images/iDempiere-Procedure-1.png)

## 步驟4, 將 ProcessedOn 按鈕與 Procedure 連結

- 點選左側 "Table and Column"
- 搜尋 "AD_User"，進入後按下下方的 "Column"
- 用 "Grid tOggle" 切換到列表模式，找到 "ProcessedOn"
- 點選 "ProcessedOn" 然後用 "Grid tOggle" 切換回表單模式
- 在 "Process" 中輸入 "hr_good_sp_hr_good_sp"
- 儲存

![Procedure-2.png](/images/iDempiere-Procedure-2.png)

