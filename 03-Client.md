# Client

## 建立帳號 (Client)

建立 Client 的步驟，從影片的 21 分 30 秒開始，下方的連結打開之後之後會直接從 21 分 30 秒開始。

https://www.youtube.com/watch?v=djb6pwZRxwg#t=21m30

### 步驟 1

登入 SuperUser

### 步驟 2

- 在上方搜尋框搜尋 "Client"，然後選擇 "Initial Client Setup Process"
- 輸入表單:

![client-1.png](/images/iDempiere-client-1.png)

- 填入表單的資料，並且勾選 "Set initial Password"，有勾這個選項的話 password = 帳號.
- 下方全部的權限都要勾選。
- 儲存

成功後會顯示：


```
** OK
Client=中油 Organization=HQ Role=中油 Admin Role=中油 User User/Contact=OilAdmin/OilAdmin User/Contact=OilUser/OilUser
Calendar=中油 Calendar Element=中油 Account Account Element # 55 Accounting Schema=中油 UN/35 
New Taiwan Dollar Acct.Schema Element=Organization Acct.Schema Element=Account Acct.Schema Element=Product 
Acct.Schema Element=Sales Region Acct.Schema Element=Project Acct.Schema Element=BPartner 
Acct.Schema Element=Campaign ---- Campaign=Standard Sales Region=Standard Activity=Standard Business Partner 
Group=Standard Business Partner=Standard Product Category=Standard Tax=Standard Product=Standard Sales 
Representative=OilUser Sales Representative=OilAdmin Project=Standard Cash Book=Standard 
```