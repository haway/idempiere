# 建立通訊錄

點選影片即可在 1:08:41 處開始學習建立通訊錄
https://www.youtube.com/watch?v=djb6pwZRxwg#t=1h08m41s

Start at 1:08:41


## 步驟1, 登入

使用 SuperUser 登入，請選擇英文界面。

## 步驟 2, 新增資料表中的欄位

- 按下左側選單的 "Table and Colunm"
- 在 "DB Table Name" 中輸入 "AD_User"
![addressbook-1.png](/images/iDempiere-Addressbook-1.png)
- 點選左下方的 "Column"
![addressbook-2.png](/images/iDempiere-Addressbook-2.png)
- 點選中間工具列的 "New" 來新增欄位
![addressbook-3.png](/images/iDempiere-Addressbook-3.png)

- "System Element" 輸入 Counter，按下 Enter 之後會另開一個選擇視窗，在選擇一次 Counter 然後按 OK
- 儲存
- 之後再按下表單內的 "Synchronize Column" 按鈕來同步資料庫

![addressbook-4.png](/images/iDempiere-Addressbook-4.png)


## 步驟 3, 新增第二個欄位

- 在步驟 2 的同一個表單內
- 點選中間工具列的 "New" 來新增欄位
- "System Element" 請選擇 "ProcessedOn"
- "Reference" 請選擇 "Button"
- "Length" 請選擇 1
- 儲存
- 之後再按下表單內的 "Synchronize Column" 按鈕來同步資料庫

![addressbook-5.png](/images/iDempiere-Addressbook-5.png)


## 步驟 4, 建立表單

- 點選左側選單的 "Window, Tab Field"
- 點選中間偏左的 "New" 來新增表單
- "Name" 請輸入 "Address Book"
- "Window Type" 請選 "Query Only" (但後來在下一張節的時候會回來改成 "Maintain")
- 輸入下方的 "Window Translation" 的翻譯名稱
- 儲存

![addressbook-7.png](/images/iDempiere-Addressbook-7.png)



## 步驟 5, 將表單與資料庫連結

- Click on "Window, Tab Field" on left menu.
- 點選左側選單的 "Window, Tab Field"
- 搜尋 "Address Book"
- 有搜尋到的話再按下下方的 "Tab"
- 在 "Table" 中輸入 "AD_User_User/Contact"
- 在 "Name" 中輸入 "User List"
- 取消 "Single Row Layout"
- 在這邊的步驟，Ray 分成了兩個部分，一個是手動增加欄位，另一個是自動新增所有欄位，他先用手動增加的方式加了 Staff ID 欄位, 然後用自動新增了所有欄位，再刪去不需要的部份。

### 手動增加欄位

- 按下下方的 "Field"，然後新增
- 在 "Column" 欄位中 輸入 "Value_Search_Key"
- 按下 Enter 後，再把 "Name" 欄位修改成 "Staff ID"
- 再次儲存

![Addressbook-10.png](/images/iDempiere-Addressbook-10.png)

### 自動增加所有欄位

- 按下表單的 "Create Fields"
- 然後會跳出一個視窗, 請在 "Entity Type" 選擇 "Dictionary" 後，按下 OK 就可以匯入所有欄位
- 接者把不要的刪掉，只要留 Staff ID, Counter, Processed On, Name, (其他看你自己)
- 你可以透過下方的 "Grid Sequence" 來排序表單內欄位的順序。
- 儲存

![Addressbook-11.png](/images/iDempiere-Addressbook-11.png)


## 步驟 6, 把通訊錄掛到選單裡面

- 點選左邊選單的 "Menu"
- 按下 "New" 新增選單
- 在 "Name" 輸入 "User List"
- 在 "Action" 輸入 "Window"
- 在 "Window" 輸入 "Address Book"
- 儲存
- 移動 Menu 到適合的位置

![Menu-1.png](/images/iDempiere-Menu-1.png)


