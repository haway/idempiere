# iDempiere Developer

## How To Join iDempiere Developer

https://www.youtube.com/watch?v=4hevXs10ndw

https://idempiere.atlassian.net/secure/Dashboard.jspa

## Develop iDempierer in localhost

https://www.youtube.com/watch?v=4hevXs10ndw

四大務必要了解的東西：

- Process：大多數的程式執行方式，Procedure 與 Java。
- Callout：事件觸發器
- iModelFactory：單據物件(採購單、Invoice、Payment)，當你要 Overwrite 這些物件的時候使用，繼承後再調整，升級也不會有問題。
- iDocFactory：會計過帳物件

## Bundle

https://www.youtube.com/watch?v=4hevXs10ndw&feature=youtu.be&t=27m45s

Bundle 裡面有 callout、Model、Process 等等，就是一個組合的意思，[影片的 26 分 28 秒](https://www.youtube.com/watch?v=4hevXs10ndw&feature=youtu.be&t=26m28s)，有示範 Bundle 與 Callout、Model、Process 的實際關係。

Bundle 就是一個 plugin，建完一個 Bundle 要做轉換。

### 建立 Bundle

https://www.youtube.com/watch?v=v8m0rfJe0g4

File -> New -> Other -> Plug-in Development -> Plug-in Project

- Project name: "tw.haway.testing"
- unCheck "Use default location"
- Choose a location (split with your idempiere). 可以先選擇另外一個專門存放的資料夾
- "an OSGi framework": "Equinox"
- Next

- Execution Enviroment: older then "JavaSE-1.7"
- Next
- Uncheck "Create a plug-in using one of the templates"
- Next


### Convent to Maven Project

- Right click on the Bundle then Configure -> Convert to Maven Project (Start form 6.2)
- Version": "(自訂)"
- Finish
- Click on pom.xml in the bundle.
- Add. 修改一下 relativePath 自己的路徑，放在第一層：

```
    <parent>
        <groupId>org.idempiere</groupId>
	    <artifactId>org.idempiere.parent</artifactId>
	    <version>6.2.0-SNAPSHOT</version>
	    <relativePath>../project/idempiereTG/org.idempiere.parent/pom.xml</relativePath>
    </parent>
```

### Export Bundle

- Click MAIN-INF -> MANIFEST.MF in your bundle
- Click on "Export Wizard".
- Choose your bundle
- Finish
- (iDempiere_root)/plug-in/plugins/(yourbundlename).jar

### Enable Bundles

- Run -> Run Configrues -> Plug-ins -> Check your bundle
- "Default": 2
- "Auto-Start": True


### Install Bundle in another iDempiere

待補

## Process

https://www.youtube.com/watch?v=RgotAYza5tE&feature=youtu.be

兩種開發方式：

- Adempiere Java Way
- IProcessFactory

Process 的應用

- Window(Button)
- Menu Process
- Workflow Process
- Scheduler Process

### Adempiere Java Way

- Right Click in src, and create a new package(tw.haway.testing.process).
- Right Click in tw.haway.teseting.process then New -> Class(Debuger).
- "SuperClass": SvrProcess

- Moidfy "import SvrProcess" to "import org.compiere.process.SvrProcess";
- Add functions automatically.
- Add Codes below:

```
protected String doIt() throws Exception {
    System.out.println("HaWay Test");
    return null;
    }
```

### 註冊 Process (Adempiere Java Way)

- Click on MAIN-INF -> MAINIFEST.MF -> Extenstions -> Add -> (org.adempiere.base.Process)
- Right clieck on org.adempiere.base.Process -> new -> Process
- "class": tw.haway.testing.debuger
- Copy "tw.haway.testing.Debuger" and paster in ID & Name of "Org.adempiere.base.Process"

### 修改 MANIFEST.MF

- Click on "MANIFEST.MF", Open "MANIFEST.MF" Tab
- paste "Eclipse-Registerbuddy: org.adempiere.base" in it.

### 放入 iDempiere

- Login in iDempiere with SuperUser
- Repoert & Process
- "Name":debuger
- "Classname": tw.haway.testing.Debuger
- New Menu
- "Name":Debuger
- "Action":Process
- "Process":"debuger_debuger"




