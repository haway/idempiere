# Pack In/Out

日期：2019-04-21

主題：Pack In/Out

你可以把做好的 Table、Window、Process 等等的打包成一個檔，然後傳到目的地去匯入。

https://www.youtube.com/watch?v=YIsPKj5tXY0&feature=youtu.be

## pack out

- Name of package:HR Package
- Package Version: 1.0

- Package Details
- New: Type->Table
- New: Type->Window
- 把你修改的 Window、Process、Table 都新增進去 Package Details
- 按下 Export Package 即可

每次修改上面的東西，都 Pack out 一次。

## Pack in

按上方迴紋針選擇 pack-out.zip 的檔案。